package com.sample.weather.listeners;

import android.net.Uri;

import com.sample.weather.data.WeatherData;

/**
 * Created by Yousuf on 11/7/15.
 */
public interface OnWeatherItemSelectedListener {
    public void onItemSelected(WeatherData data);
}
