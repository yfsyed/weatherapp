package com.sample.weather.listeners;

import android.view.View;

/**
 * Created by A704968 on 11/7/15.
 */
public interface OnItemClickListener {
    void onItemClickListener(View v, int index);
}
