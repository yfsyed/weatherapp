package com.sample.weather.activity;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.sample.weather.R;
import com.sample.weather.WeatherApplication;
import com.sample.weather.data.WeatherAPIResults;
import com.sample.weather.data.WeatherData;
import com.sample.weather.fragment.WeatherDetailsFragment;
import com.sample.weather.fragment.WeatherListFragment;
import com.sample.weather.listeners.OnWeatherItemSelectedListener;
import com.sample.weather.utils.AppConstants;
import com.sample.weather.weather_request.WeatherDataRequest;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class WeatherDisplayActivity extends AppCompatActivity implements OnWeatherItemSelectedListener, Response.ErrorListener, Response.Listener<JSONObject> {

    private List<WeatherData> mWeatherDataList;
    private boolean mSavedInstance;
    private boolean mMultiView;
    private ProgressBar mDataProgress;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_display);
        mDataProgress = (ProgressBar) findViewById(R.id.progress_weather_data_request);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if (savedInstanceState != null) {
            mWeatherDataList = savedInstanceState.getParcelableArrayList(AppConstants.WEATHER_DATA);
            mSavedInstance = true;
        }
        mMultiView = (findViewById(R.id.weather_details_container) != null);
        loadWeatherData();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(AppConstants.WEATHER_DATA, (ArrayList) mWeatherDataList);
    }

    @Override
    public void onBackPressed() {
        if ((!mMultiView && (getFragmentManager().getBackStackEntryCount() > 0))) {
            getFragmentManager().popBackStackImmediate();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_weather_display, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                fetchWeatherData();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResponse(JSONObject response) {
        try {
            WeatherAPIResults results = new WeatherAPIResults((response));
            mWeatherDataList = results.getWeatherData();
            WeatherApplication.getInstance().saveRequestTimeStamp();
            updateWeatherDataFragments();
        } catch (Exception e) {
            displayError(e.getMessage());
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        displayError(error.getMessage());
    }

    @Override
    public void onItemSelected(WeatherData data) {
        WeatherDetailsFragment detailFragment;
        if (mMultiView) {
            detailFragment = (WeatherDetailsFragment) getFragmentManager().findFragmentById(R.id.weather_details_container);
            detailFragment.updateWeatherDetails(data);
        } else {
            detailFragment = new WeatherDetailsFragment();
            detailFragment.setWeatherDetails(data);
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.weather_list_container, detailFragment, WeatherDetailsFragment.TAG);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(WeatherDetailsFragment.class.getSimpleName());
            ft.commit();
        }
    }

    private void loadWeatherData() {
        if (!mSavedInstance || mWeatherDataList == null || !WeatherApplication.getInstance().isWeatherDataValid()) {
            fetchWeatherData();
        } else {
            updateWeatherDataFragments();
        }
    }

    private void fetchWeatherData(){
        mDataProgress.setVisibility(View.VISIBLE);
        WeatherDataRequest request = new WeatherDataRequest(
                Request.Method.GET, AppConstants.getWeatherUrl(), null, this, this);
        WeatherApplication.getInstance().add(request);
    }

    private void displayError(String message) {
        mDataProgress.setVisibility(View.GONE);
        Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG).show();
    }

    public void updateWeatherDataFragments() {
        mDataProgress.setVisibility(View.GONE);
        displayWeatherListContainer();

        if (mMultiView) {
            displayWeatherDetailsContainer();
        }
    }

    public void displayWeatherListContainer() {
        if (!mSavedInstance) {
            getFragmentManager().popBackStack();
            WeatherListFragment listFragment = new WeatherListFragment();
            listFragment.setWeatherData(mWeatherDataList);
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.weather_list_container, listFragment, WeatherListFragment.TAG);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.commit();
        } else {
            Fragment fragment = getFragmentManager().findFragmentById(R.id.weather_list_container);
            if(fragment instanceof WeatherDetailsFragment){
                getFragmentManager().popBackStack();
            }
        }
    }

    private void displayWeatherDetailsContainer() {
        WeatherDetailsFragment detailFragment = (WeatherDetailsFragment) getFragmentManager().findFragmentById(R.id.weather_details_container);
        if (detailFragment == null) {
            detailFragment = new WeatherDetailsFragment();
            detailFragment.setWeatherDetails(getWeatherDetails(0));
        }

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.weather_details_container, detailFragment, WeatherDetailsFragment.TAG);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(WeatherDetailsFragment.class.getSimpleName());
        ft.commit();
    }

    private WeatherData getWeatherDetails(int index) {
        if (mWeatherDataList != null && mWeatherDataList.size() > index) {
            return mWeatherDataList.get(index);
        }
        return null;
    }


}
