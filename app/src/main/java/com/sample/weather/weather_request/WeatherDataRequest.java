package com.sample.weather.weather_request;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

/**
 * Created by Yousuf Syed on 11/8/15.
 */
public class WeatherDataRequest extends JsonObjectRequest{

    public WeatherDataRequest(int method, String url,
                              JSONObject jsonRequest,
                              Response.Listener<JSONObject> listener,
                              Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }

    @Override
    public Priority getPriority() {
        return Priority.NORMAL;
    }
}