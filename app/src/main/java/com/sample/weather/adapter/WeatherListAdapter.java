package com.sample.weather.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sample.weather.R;
import com.sample.weather.data.WeatherData;
import com.sample.weather.data.WeatherDetails;
import com.sample.weather.listeners.OnItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Yousuf Syed on 11/7/15.
 */
public class WeatherListAdapter extends RecyclerView.Adapter<WeatherListAdapter.WeatherInfoHolder>{

    private Context mContext;
    private List<WeatherData> mWeatherDataList;
    private OnItemClickListener mItemClickListener;

    public WeatherListAdapter(Context ctx, List<WeatherData> weatherDataList){
        mWeatherDataList = weatherDataList;
        mContext = ctx;
    }

    public void setItemClickListener(OnItemClickListener listener){
        mItemClickListener = listener;
    }

    @Override
    public WeatherInfoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_info_item, parent, false);
        return new WeatherInfoHolder(view);
    }

    @Override
    public void onBindViewHolder(WeatherInfoHolder holder, int position) {
        WeatherData data = mWeatherDataList.get(position);
        holder.mTemperature.setText(Html.fromHtml(mContext.getString(R.string.weather_temp_day, data.getDay())));
        holder.mHumidity.setText(Html.fromHtml(mContext.getString(R.string.weather_humidity, data.getHumidity())));
        if(data.getWeatherDetails() != null && !data.getWeatherDetails().isEmpty()) {
            WeatherDetails details = data.getWeatherDetails().get(0);

            holder.mDescription.setText(details.getDescription());
            String iconUrl = details.getIcon();
            if(!TextUtils.isEmpty(iconUrl)) {
                Picasso.with(mContext)
                        .load(details.getIcon())
                        .into(holder.mIcon);

            }
        }
    }

    @Override
    public int getItemCount() {
        if(mWeatherDataList == null) {
            return 0;
        }
        return mWeatherDataList.size();
    }

    public class WeatherInfoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        protected TextView mTemperature, mHumidity, mDescription;
        protected ImageView mIcon;

        public WeatherInfoHolder(View itemView) {
            super(itemView);
            mTemperature = (TextView) itemView.findViewById(R.id.temperature);
            mHumidity = (TextView) itemView.findViewById(R.id.humidity);
            mDescription = (TextView) itemView.findViewById(R.id.description);
            mIcon = (ImageView) itemView.findViewById(R.id.weather_icon);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mItemClickListener != null){
                mItemClickListener.onItemClickListener(v, getLayoutPosition());
            }
        }
    }




}
