package com.sample.weather.utils;

import com.sample.weather.data.WeatherAPIResults;
import com.sample.weather.data.WeatherData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by A704968 on 11/7/15.
 */
public class AppConstants {

    public static final String WEATHER_DATA = "weather-list";

    public static final String WEATHER_DETAILS = "weather_details";

    public static final String PREF_DATA_REQUEST_TIME = "weather-data-expiry-timer-prefs";

    public static final String ICON_URL = "http://openweathermap.org/img/w/%s.png";

    public static final String API_KEY = "5051fde67d2d8bd6d163241d59b5a61c";

    public static final String WEATHER_API_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?q=Philadelphia&mode=json&units=metric&cnt=16&APPID=%s";

    public static String getWeatherUrl() {
        return String.format(WEATHER_API_URL, API_KEY);
    }

}
