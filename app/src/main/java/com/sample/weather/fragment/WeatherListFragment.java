package com.sample.weather.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sample.weather.R;
import com.sample.weather.adapter.WeatherListAdapter;
import com.sample.weather.data.WeatherData;
import com.sample.weather.listeners.OnItemClickListener;
import com.sample.weather.listeners.OnWeatherItemSelectedListener;
import com.sample.weather.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment to display list of weather results for the past 16 days.
 */
public class WeatherListFragment extends Fragment implements OnItemClickListener {

    public static final String TAG = WeatherDetailsFragment.class.getSimpleName();

    private List<WeatherData> mWeatherDataList;
    private OnWeatherItemSelectedListener mListener;

    public WeatherListFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            mWeatherDataList = savedInstanceState.getParcelableArrayList(AppConstants.WEATHER_DETAILS);
        }
        initRecyclerView(getView());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View root = inflater.inflate(R.layout.fragment_weather_display, container, false);
        return root;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(AppConstants.WEATHER_DETAILS, (ArrayList) mWeatherDataList);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnWeatherItemSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnURLSelectedListener");
        }
    }

    @Override
    public void onDetach() {
        mListener = null;
        super.onDetach();
    }

    public void setWeatherData(List<WeatherData> data) {
        mWeatherDataList = data;
    }

    private void initRecyclerView(View root) {
        RecyclerView weatherList = (RecyclerView) root.findViewById(R.id.weather_list);
        weatherList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        weatherList.setLayoutManager(llm);
        WeatherListAdapter weatherAdapter = new WeatherListAdapter(getActivity(), mWeatherDataList);
        weatherAdapter.setItemClickListener(this);
        weatherList.setAdapter(weatherAdapter);
    }

    @Override
    public void onItemClickListener(View v, int index) {
        if (mListener != null) {
            mListener.onItemSelected(mWeatherDataList.get(index));
        }
    }
}
