package com.sample.weather.fragment;

import android.os.Bundle;
import android.app.Fragment;
import android.text.Html;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sample.weather.R;
import com.sample.weather.data.WeatherData;
import com.sample.weather.data.WeatherDetails;
import com.sample.weather.utils.AppConstants;

import java.util.Date;

/**
 * Fragment to display Weather details
 */
public class WeatherDetailsFragment extends Fragment {

    public static final String TAG = WeatherDetailsFragment.class.getSimpleName();

    private TextView mDayText, mMinText, mMaxText;
    private TextView mIdText, mContentText, mDescText;
    private TextView mDateText, mDegreeText, mCloudsText;
    private TextView mMorningText, mEveningText, mNightText;
    private TextView mSpeedText, mRainText, mPressureText, mHumidityText;

    private WeatherData mWeatherData;

    public WeatherDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            mWeatherData = savedInstanceState.getParcelable(AppConstants.WEATHER_DETAILS);
        }
        initViews(getView());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_weather_details, container, false);
        return root;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(AppConstants.WEATHER_DETAILS, mWeatherData);
    }

    public void setWeatherDetails(WeatherData data) {
        mWeatherData = data;
    }

    public void updateWeatherDetails(WeatherData data) {
        mWeatherData = data;
        initViews(getView());
    }

    private void initViews(View root) {
        mDateText = (TextView) root.findViewById(R.id.weather_date);
        mIdText = (TextView) root.findViewById(R.id.weather_id);
        mContentText = (TextView) root.findViewById(R.id.weather_content);
        mDescText = (TextView) root.findViewById(R.id.weather_description);

        mMorningText = (TextView) root.findViewById(R.id.weather_temp_morning);
        mEveningText = (TextView) root.findViewById(R.id.weather_temp_eve);
        mNightText = (TextView) root.findViewById(R.id.weather_temp_night);
        mDayText = (TextView) root.findViewById(R.id.weather_temp_day);
        mMaxText = (TextView) root.findViewById(R.id.weather_temp_max);
        mMinText = (TextView) root.findViewById(R.id.weather_temp_min);

        mDegreeText = (TextView) root.findViewById(R.id.weather_degree);
        mCloudsText = (TextView) root.findViewById(R.id.weather_clouds);
        mSpeedText = (TextView) root.findViewById(R.id.weather_speed);
        mRainText = (TextView) root.findViewById(R.id.weather_rain);
        mPressureText = (TextView) root.findViewById(R.id.weather_pressure);
        mHumidityText = (TextView) root.findViewById(R.id.weather_humidity);

        if (mWeatherData != null) {
            Date weatherDate = new Date(mWeatherData.getDate() * 1000);
            CharSequence dateString = DateFormat.format("EEEE, MMMM dd", weatherDate);

            mDateText.setText(Html.fromHtml(getString(R.string.weather_date, dateString)));
            mMorningText.setText(Html.fromHtml(getString(R.string.weather_temp_morning, mWeatherData.getMorning())));
            mEveningText.setText(Html.fromHtml(getString(R.string.weather_temp_eve, mWeatherData.getEvening())));
            mNightText.setText(Html.fromHtml(getString(R.string.weather_temp_night, mWeatherData.getNight())));

            mHumidityText.setText(Html.fromHtml(getString(R.string.weather_humidity, mWeatherData.getHumidity())));
            mPressureText.setText(Html.fromHtml(getString(R.string.weather_pressure, mWeatherData.getPressure())));
            mSpeedText.setText(Html.fromHtml(getString(R.string.weather_speed, mWeatherData.getSpeed())));
            mRainText.setText(Html.fromHtml(getString(R.string.weather_rain, mWeatherData.getRain())));
            mCloudsText.setText(Html.fromHtml(getString(R.string.weather_clouds, mWeatherData.getClouds())));
            mDegreeText.setText(Html.fromHtml(getString(R.string.weather_degree, mWeatherData.getDegrees())));
            mMinText.setText(Html.fromHtml(getString(R.string.weather_temp_min, mWeatherData.getMin())));
            mMaxText.setText(Html.fromHtml(getString(R.string.weather_temp_max, mWeatherData.getMax())));
            mDayText.setText(Html.fromHtml(getString(R.string.weather_temp_day, mWeatherData.getDay())));

            if (mWeatherData.getWeatherDetails() != null && !mWeatherData.getWeatherDetails().isEmpty()) {
                WeatherDetails details = mWeatherData.getWeatherDetails().get(0);
                mIdText.setText(Html.fromHtml(getString(R.string.weather_id, details.getId())));
                mContentText.setText(Html.fromHtml(getString(R.string.weather_content, details.getMainContent())));
                mDescText.setText(Html.fromHtml(getString(R.string.weather_description, details.getDescription())));
            }
        }
    }

}
