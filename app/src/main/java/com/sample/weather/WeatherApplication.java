package com.sample.weather;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.sample.weather.utils.AppConstants;
import com.sample.weather.utils.DateUtils;

import java.util.Date;

/**
 * Created by A704968 on 11/8/15.
 */
public class WeatherApplication extends Application {

    private RequestQueue mRequestQueue;
    private static WeatherApplication mInstance;
    private static final String TAG = WeatherApplication.class.getName();

    public static synchronized WeatherApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        mRequestQueue = Volley.newRequestQueue(getApplicationContext());
    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

    public <T> void add(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancel() {
        mRequestQueue.cancelAll(TAG);
    }

    /**
     * Save timestamp for the network request so that we can make request
     */
    public void saveRequestTimeStamp(){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.edit().putLong(AppConstants.PREF_DATA_REQUEST_TIME, System.currentTimeMillis()).commit();
    }

    /**
     * Check whether the existing weather data is valid of not,
     * because after 12am we might be getting the data starting from next day.
     * @return
     */
    public boolean isWeatherDataValid(){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        long dateValue = sp.getLong(AppConstants.PREF_DATA_REQUEST_TIME, 0);

        Date requestDate = new Date(dateValue);
        Date endDate = DateUtils.getEndDate(requestDate);
        if(endDate == DateUtils.max(requestDate,endDate)){
            return true;
        }
        return false;
    }


}
