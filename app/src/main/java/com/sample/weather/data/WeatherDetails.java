package com.sample.weather.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.sample.weather.utils.AppConstants;

import org.json.JSONObject;

/**
 * Created by A704968 on 11/7/15.
 */
public class WeatherDetails implements Parcelable {

    private int mId;
    private String mMainContent;
    private String mDescription;
    private String mIcon;

    public WeatherDetails(JSONObject jsonObject) {
        parseJson(jsonObject);
    }

    public int getId() {
        return mId;
    }

    public String getMainContent() {
        return mMainContent;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getIcon() {
        return mIcon;
    }

    /**
     * @param jsonObject
     */
    private void parseJson(JSONObject jsonObject) {
        if (jsonObject != null) {
            mId = jsonObject.optInt("id");
            mMainContent = jsonObject.optString("main");
            mDescription = jsonObject.optString("description");
            String tempIcon = jsonObject.optString("icon");
            mIcon = String.format(AppConstants.ICON_URL, tempIcon);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mMainContent);
        dest.writeString(mDescription);
        dest.writeString(mIcon);
    }

    public WeatherDetails(Parcel in) {
        mId = in.readInt();
        mMainContent = in.readString();
        mDescription = in.readString();
        mIcon = in.readString();
    }

    public static final Creator<WeatherDetails> CREATOR = new Creator<WeatherDetails>() {
        public WeatherDetails createFromParcel(Parcel in) {
            return new WeatherDetails(in);
        }

        public WeatherDetails[] newArray(int size) {
            return new WeatherDetails[size];
        }
    };
}
