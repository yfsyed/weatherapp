package com.sample.weather.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.sample.weather.utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by A704968 on 11/7/15.
 */
public class WeatherAPIResults implements Parcelable {

    private String mCityName;
    private int mCityId;
    private double mLatitude;
    private double mLongitude;
    private String mCountry;
    private long mPopulation;
    private String mCod;
    private int mCount;
    private List<WeatherData> mWeatherData;

    public String getCityName() {
        if(!TextUtils.isEmpty(mCityName))
            mCityName = "";
        return mCityName;
    }

    public int getCityId() {
        return mCityId;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public String getCountry() {
        if(!TextUtils.isEmpty(mCountry))
            mCountry = "";
        return mCountry;
    }

    public long getPopulation() {
        return mPopulation;
    }

    public String getCod() {
        if(!TextUtils.isEmpty(mCod))
            mCod = "";
        return mCod;
    }

    public int getCount() {
        return mCount;
    }

    public List<WeatherData> getWeatherData() {
        return mWeatherData;
    }

    public WeatherAPIResults(JSONObject jsonObject) {
        mWeatherData = new ArrayList<>();
        parseJson(jsonObject);
    }

    /**
     * @param jsonObject
     */
    private void parseJson(JSONObject jsonObject) {
        if (jsonObject != null) {
            parseCityJson(jsonObject.optJSONObject("city"));
            parseWeatherList(jsonObject.optJSONArray("list"));
            mCod = jsonObject.optString("cod");
            mCount = jsonObject.optInt("cnt");
        }
    }

    private void parseCityJson(JSONObject jsonObject) {
        if (jsonObject != null) {
            mCityId = jsonObject.optInt("id");
            mCityName = jsonObject.optString("name");
            mCountry = jsonObject.optString("country");
            mPopulation = jsonObject.optLong("population");
            JSONObject coordinateObject = jsonObject.optJSONObject("coord");
            if(coordinateObject != null) {
                mLatitude = coordinateObject.optDouble("lat");
                mLongitude = coordinateObject.optDouble("lon");
            }
        }
    }

    private void parseWeatherList(JSONArray jsonArray){
        if(jsonArray != null) {
            for (int i = 0; i < jsonArray.length(); i++) {
                WeatherData data = new WeatherData(jsonArray.optJSONObject(i));
                mWeatherData.add(data);
            }
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mCityId);
        dest.writeString(mCityName);
        dest.writeString(mCountry);
        dest.writeLong(mPopulation);
        dest.writeDouble(mLatitude);
        dest.writeDouble(mLongitude);
        dest.writeString(mCod);
        dest.writeInt(mCount);

        dest.writeList(mWeatherData);

    }

    public WeatherAPIResults(Parcel in) {
        mCityId = in.readInt();
        mCityName = in.readString();
        mCountry = in.readString();
        mPopulation = in.readLong();

        mLatitude = in.readDouble();
        mLongitude = in.readDouble();
        mCod = in.readString();
        mCount = in.readInt();

        in.readList(mWeatherData, WeatherData.class.getClassLoader());
    }

    public static final Creator<WeatherAPIResults> CREATOR = new Creator<WeatherAPIResults>() {
        public WeatherAPIResults createFromParcel(Parcel in) {
            return new WeatherAPIResults(in);
        }

        public WeatherAPIResults[] newArray(int size) {
            return new WeatherAPIResults[size];
        }
    };
}
