package com.sample.weather.data;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yousuf Syed on 11/7/15.
 */
public class WeatherData implements Parcelable {

    private long mDate;
    private double mPressure;
    private int mHumidity;
    private double mSpeed;
    private double mRain;
    private int mDegrees;
    private int mClouds;

    private double mDay;
    private double min;
    private double max;
    private double mNight;
    private double mEvening;
    private double mMorning;

    private List<WeatherDetails> mWeatherDetails;

    public long getDate() {
        return mDate;
    }

    public double getPressure() {
        return mPressure;
    }

    public int getHumidity() {
        return mHumidity;
    }

    public double getSpeed() {
        return mSpeed;
    }

    public double getRain() {
        return mRain;
    }

    public float getDegrees() {
        return mDegrees;
    }

    public int getClouds() {
        return mClouds;
    }

    public double getDay() {
        return mDay;
    }

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }

    public double getNight() {
        return mNight;
    }

    public double getEvening() {
        return mEvening;
    }

    public double getMorning() {
        return mMorning;
    }

    public List<WeatherDetails> getWeatherDetails() {
        return mWeatherDetails;
    }

    public WeatherData(JSONObject jsonObject) {
        mWeatherDetails = new ArrayList();
        parseJson(jsonObject);
    }

    /**
     * @param jsonObject
     */
    private void parseJson(JSONObject jsonObject) {
        if (jsonObject != null) {
            mDate = jsonObject.optLong("dt");
            mPressure = jsonObject.optDouble("pressure");
            mHumidity = jsonObject.optInt("humidity");
            mSpeed = jsonObject.optDouble("speed");
            mRain = jsonObject.optDouble("rain");
            mDegrees = jsonObject.optInt("deg");
            mClouds = jsonObject.optInt("clouds");

            parseTemperatureInfo(jsonObject);
            parseWeatherInfo(jsonObject);
        }
    }

    /**
     * @param jsonObject
     */
    private void parseTemperatureInfo(JSONObject jsonObject) {
        JSONObject temperatureJSON = jsonObject.optJSONObject("temp");
        if (temperatureJSON != null) {
            mDay = temperatureJSON.optDouble("day");
            min = temperatureJSON.optDouble("min");
            max = temperatureJSON.optDouble("max");
            mNight = temperatureJSON.optDouble("night");
            mEvening = temperatureJSON.optDouble("eve");
            mMorning = temperatureJSON.optDouble("morn");
        }
    }

    /**
     * @param jsonObject
     */
    private void parseWeatherInfo(JSONObject jsonObject) {
        JSONArray weatherDetailsObject = jsonObject.optJSONArray("weather");
        if (weatherDetailsObject != null) {
            WeatherDetails weatherDetail = null;
            for (int i = 0; i < weatherDetailsObject.length(); i++) {
                JSONObject weatherInfo = weatherDetailsObject.optJSONObject(i);
                weatherDetail = new WeatherDetails(weatherInfo);
                mWeatherDetails.add(weatherDetail);
            }
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mDate);
        dest.writeDouble(mPressure);
        dest.writeInt(mHumidity);
        dest.writeDouble(mSpeed);
        dest.writeDouble(mRain);
        dest.writeInt(mDegrees);
        dest.writeInt(mClouds);

        dest.writeDouble(mDay);
        dest.writeDouble(min);
        dest.writeDouble(max);
        dest.writeDouble(mNight);
        dest.writeDouble(mEvening);
        dest.writeDouble(mMorning);

        dest.writeList(mWeatherDetails);
    }

    public WeatherData(Parcel in) {
        mDate = in.readLong();
        mPressure = in.readDouble();
        mHumidity = in.readInt();
        mSpeed = in.readDouble();
        mRain = in.readDouble();
        mDegrees = in.readInt();
        mClouds = in.readInt();

        mDay = in.readDouble();
        min = in.readDouble();
        max = in.readDouble();
        mNight = in.readDouble();
        mEvening = in.readDouble();
        mMorning = in.readDouble();

        in.readList(mWeatherDetails, WeatherDetails.class.getClassLoader());
    }

    public static final Creator<WeatherData> CREATOR = new Creator<WeatherData>() {
        public WeatherData createFromParcel(Parcel in) {
            return new WeatherData(in);
        }

        public WeatherData[] newArray(int size) {
            return new WeatherData[size];
        }
    };
}
